BUILD=/home/rikisa/src/tfubuntu/build
ROOT=$BUILD/chroot
HOSTNAME=UbunutTF

# dirs for livebooting and installler
mkdir -p $BUILD/image/{casper,isolinux,install}

sudo cp $ROOT/boot/vmlinuz-**-**-generic $BUILD/image/casper/vmlinuz
sudo cp $ROOT/boot/initrd.img-**-**-generic $BUILD/image/casper/initrd
# memtest BIOS
sudo cp $ROOT/boot/memtest86+.bin $BUILD/image/install/memtest86+
# memtest UEFI
wget --progress=dot https://www.memtest86.com/downloads/memtest86-usb.zip -O $BUILD/image/install/memtest86-usb.zip
unzip -p $BUILD/image/install/memtest86-usb.zip memtest86-usb.img > $BUILD/image/install/memtest86
rm -f $BUILD/image/install/memtest86-usb.zip

### Setup GRUB
# Create base point access file for grub
touch $BUILD/image/ubuntu

# grub config
cat <<EOF > $BUILD/image/isolinux/grub.cfg

search --set=root --file /ubuntu

insmod all_video

set default="0"
set timeout=30

menuentry "Try Ubuntu FS without installing" {
   linux /casper/vmlinuz boot=casper nopersistent toram quiet splash ---
   initrd /casper/initrd
}

menuentry "Install Ubuntu FS" {
   linux /casper/vmlinuz boot=casper only-ubiquity quiet splash ---
   initrd /casper/initrd
}

menuentry "Check disc for defects" {
   linux /casper/vmlinuz boot=casper integrity-check quiet splash ---
   initrd /casper/initrd
}

menuentry "Test memory Memtest86+ (BIOS)" {
   linux16 /install/memtest86+
}

menuentry "Test memory Memtest86 (UEFI, long load time)" {
   insmod part_gpt
   insmod search_fs_uuid
   insmod chain
   loopback loop /install/memtest86
   chainloader (loop,gpt1)/efi/boot/BOOTX64.efi
}
EOF

### Create File Manifest for installer
sudo chroot $ROOT dpkg-query -W --showformat='${Package} ${Version}\n' | sudo tee $BUILD/image/casper/filesystem.manifest

sudo cp -v $BUILD/image/casper/filesystem.manifest $BUILD/image/casper/filesystem.manifest-desktop

# remove live files from manifest
sudo sed -i '/ubiquity/d' $BUILD/image/casper/filesystem.manifest-desktop
sudo sed -i '/casper/d' $BUILD/image/casper/filesystem.manifest-desktop
sudo sed -i '/discover/d' $BUILD/image/casper/filesystem.manifest-desktop
sudo sed -i '/laptop-detect/d' $BUILD/image/casper/filesystem.manifest-desktop
sudo sed -i '/os-prober/d' $BUILD/image/casper/filesystem.manifest-desktop
