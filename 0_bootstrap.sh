# Mostly taken from https://itnext.io/how-to-create-a-custom-ubuntu-live-from-scratch-dd3b3f213f81

BUILD=/home/rikisa/src/tfubuntu/build
ROOT=$BUILD/chroot
HOSTNAME=UbunutTF

echo "Creating root dir in $ROOT"
# sudo mkdir -p $BUILD
mkdir -p $ROOT

echo "Bootstraping Ubuntu base system"
# create ubuntu environment without apt from sources
sudo debootstrap --arch=amd64 --variant=minbase focal $ROOT http://de.archive.ubuntu.com/ubuntu/

echo "Tunneling through system"
# gracefully let the chrotted ubuntu use my host hardware ;)
sudo mount --bind /dev $ROOT/dev
sudo mount --bind /run $ROOT/run

echo "Setting up buildscript for chroot"
sudo cp 1.**.sh $ROOT/root

echo "Switching into root jail"
### chroot, run script 1_build.sh
sudo chroot $ROOT
