BUILD=/home/rikisa/src/tfubuntu/build
ROOT=$BUILD/chroot
HOSTNAME=UbunutTF

# remove build script
sudo rm -v \
    $ROOT/root/1.0_setupsys.sh \
    $ROOT/root/1.2_cleanup.sh \
    $ROOT/root/1.1_tensorflow.sh cuda-repo-ubuntu2004-11-3-local_11.3.1-465.19.01-1_amd64.deb    
# 
# # umount system
sudo umount $ROOT/dev
sudo umount $ROOT/run
