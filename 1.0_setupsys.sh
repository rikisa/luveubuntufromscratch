# Mostly taken from https://itnext.io/how-to-create-a-custom-ubuntu-live-from-scratch-dd3b3f213f81

BUILD=/home/rikisa/src/tfubuntu/build
ROOT=$BUILD/chroot
HOSTNAME=UbunutTF

# not set, but must otherwise systemd is whiny about unsafe path transitions
chown root:root /

# set default path environment, as somehow chroot does not change that
export PATH=/usr/sbin:/usr/bin:/sbin:/bin

# in rootjail
mount none -t proc /proc
mount none -t sysfs /sys
mount none -t devpts /dev/pts

export HOME=/root # happens automatically, but lets be save
export LC_ALL=C
echo $HOSTNAME > /etc/hostname

# setup apt sources
cat <<EOF > /etc/apt/sources.list
deb http://de.archive.ubuntu.com/ubuntu/ focal main restricted universe multiverse 
deb-src http://de.archive.ubuntu.com/ubuntu/ focal main restricted universe multiverse
deb http://de.archive.ubuntu.com/ubuntu/ focal-security main restricted universe multiverse 
deb-src http://de.archive.ubuntu.com/ubuntu/ focal-security main restricted universe multiverse
deb http://de.archive.ubuntu.com/ubuntu/ focal-updates main restricted universe multiverse 
deb-src http://de.archive.ubuntu.com/ubuntu/ focal-updates main restricted universe multiverse    
EOF

# install systemd and dependencies
# systemd is not a must, dbus might be sufficient
# but as I intend to use systemd anyways... meh
apt-get update
apt-get install -y systemd-sysv

# machine uuid and let the debus know it
dbus-uuidgen > /etc/machine-id 
ln -fs /etc/machine-id /var/lib/dbus/machine-id

# diversions of packages? seesm to be needed for live usb
dpkg-divert --local --rename --add /sbin/initctl
ln -s /bin/true /sbin/initctl

# setup the live system
apt-get install -y \
    ubuntu-standard \
    casper \
    lupin-casper \
    discover \
    laptop-detect \
    os-prober \
    network-manager \
    resolvconf \
    net-tools \
    wireless-tools \
    wpagui \
    locales \
    linux-generic

# live installer
# + graphical frontend, starting at frontend
apt-get install -y \
    ubiquity \
    ubiquity-casper \
    ubiquity-frontend-gtk \
    ubiquity-slideshow-ubuntu \
    ubiquity-ubuntu-artwork

# more graphical stuff
apt-get install -y \
    plymouth-theme-ubuntu-logo \
    ubuntu-gnome-desktop \
    ubuntu-gnome-wallpapers

# some utils
apt-get install -y \
    less \
    vim-gtk3 \
    curl \
    build-essential \
    zsh

# driver cuda, etc
# sudo add-apt-repository ppa:graphics-drivers/ppa
# apt-get install -y \
#     nvidia-driver-460 \
#     nvidia-cuda-toolkit

# export LD_LIBRARY_PATH=/usr/lib/cuda/lib64/

# # setup the conda environment
adduser --quiet --gecos "" tensorflow
# wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -o /home/tensorflow/Miniconda3-latest-Linux-x86_64.sh
# chmod u+x Miniconda3-latest-Linux-x86_64.sh
# sudo ./Miniconda3-latest-Linux-x86_64.sh
# chown -R tensorflow:tensorflow /home/tensorflow/miniconda

