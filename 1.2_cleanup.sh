# Mostl ytaken from https://itnext.io/how-to-create-a-custom-ubuntu-live-from-scratch-dd3b3f213f81

BUILD=/home/rikisa/src/tfubuntu/build
ROOT=$BUILD/chroot
HOSTNAME=UbunutTF

# locales
dpkg-reconfigure locales
dpkg-reconfigure resolvconf

cat <<EOF > /etc/NetworkManager/NetworkManager.conf
[main]
rc-manager=resolvconf
plugins=ifupdown,keyfile
dns=dnsmasq
[ifupdown]
managed=false
EOF

dpkg-reconfigure network-manager

# more cleanup
truncate -s 0 /etc/machine-id
rm /sbin/initctl
dpkg-divert --rename --remove /sbin/initctl

apt-get clean
rm -rf /tmp/* ~/.bash_history
umount /proc
umount /sys
umount /dev/pts
export HISTSIZE=0
